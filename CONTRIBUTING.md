# Contributing

## Setup

### Minikube

*  install minikube
*  config minikube with a little more resources than default
   ```console
   $ minikube config set memory 4096
   $ minikube config set cpus 2
   ``` 
*  (optional) use virtualbox driver for minikube
   ```console
   $ minikube config set vm-driver virtualbox
   ```
*  start minikube (and optionally open kubernetes dashboard)  -- usato shell per block
   ```shell
   $ minikube start
   $ minikube dashboard
   ```
